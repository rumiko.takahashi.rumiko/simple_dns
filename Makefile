
NAME				:=			dns_serv
SRC_DIR			:=			./srcs/
OBJ_DIR			:=			./obj/
INC_DIR			:=			./inc/

SRC					:=			main.c dns.c

OBJ		 			=			$(addprefix $(OBJ_DIR), $(SRC:.c=.o))

HEADER_FLAGS	:=			-I $(INC_DIR)

CC				:=			gcc -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME)

$(OBJ): | $(OBJ_DIR)

$(OBJ_DIR):
	 mkdir $(OBJ_DIR)

$(OBJ_DIR)%.o: %.c
	$(CC) -c $< -o $@ $(CC_FLAGS) $(HEADER_FLAGS)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)
	rm -rf $(OBJ_DIR)

re: fclean all
vpath %.c $(SRC_DIR)
