#include "../inc/dns_server.h"

/*https://habr.com/post/141021/ */

void  start_dns_server(config_t *config)
{
  int   recv_byte;
  socklen_t cl_sockaddr_len;
  int   sockfd;
  char  buffer[MAXLINE];
  struct sockaddr_in servaddr;
  struct sockaddr_in cliaddr;
  int     send_byte;
  int     n;
  int     enable;

  if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
      perror("socket creation failed");
      exit(EXIT_FAILURE);
  }
  memset((char *)&servaddr, 0, sizeof(servaddr));
  memset((char *)&cliaddr, 0, sizeof(cliaddr));

  servaddr.sin_family = AF_INET; // IPv4
  servaddr.sin_addr.s_addr = INADDR_ANY; // lisen all interface in PC
  servaddr.sin_port = htons(PORT); // port for lisen

  /* enable use port again */
  enable = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1)
	{
    	close(sockfd);
      perror("setsockopt");
      exit(EXIT_SUCCESS);
	}

  if ( bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0 )
  {
      perror("bind failed");
      exit(EXIT_FAILURE);
  }
  printf("Start dns server\n");
  while (1)
  {
    memset(&buffer, 0, MAXLINE);
    recv_byte = recvfrom(sockfd, buffer, MAXLINE,
                  MSG_WAITALL, (struct sockaddr *) &cliaddr,
                  &cl_sockaddr_len);
    if (recv_byte == -1)
    {
      perror("Error recvfrom");
      exit(EXIT_SUCCESS);
    }
    if (recv_byte == 0)
      continue ;
    if (check_blacklist((char *)buffer, config))
    {
      printf("%s\n", "in if");
      send_rec(sockfd, buffer, recv_byte, &cliaddr);
    }
    else
    {
      n = query_resend(config, buffer, recv_byte);
      send_byte = sendto(sockfd, buffer, n, 0, (struct sockaddr *)&cliaddr, cl_sockaddr_len);
      if (send_byte == -1)
      {
        perror("sendto in start_dns_server");
        exit (EXIT_SUCCESS);
      }
    }
  }
  close(sockfd);
}

int query_resend(config_t *data, char *buff, int rec_byte)
{
  int sockfd;
  struct sockaddr_in dest;
  socklen_t dest_len_adrr;

  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
  {
    perror("socket in query_resend");
    exit (EXIT_SUCCESS);
  }
  bzero(&dest, sizeof(dest));
  printf("data->forwarder == %s\n", data->forwarder);
  dest.sin_family = AF_INET;
  dest. sin_addr.s_addr = inet_addr(data->forwarder);
  dest.sin_port = htons(PORT);
  dest_len_adrr = sizeof(dest);
  if (sendto(sockfd, buff, rec_byte, 0, (struct sockaddr *)&dest, sizeof(dest)) == -1)
  {
    perror("sendto in query_resend");
    exit (EXIT_SUCCESS);
  }
  rec_byte = recvfrom(sockfd, buff, MAXLINE, 0, (struct sockaddr *)&dest, &dest_len_adrr);
  if (rec_byte == -1)
  {
    perror("recvfrom in query_resend");
    exit (EXIT_SUCCESS);
  }
  close(sockfd);
  return (rec_byte);
}

void send_rec(int sockfd, char *buffer, int len_recv, struct sockaddr_in *client)
{
  t_dns_header  *dns_header;
  socklen_t     len_clie_addr;
  int           send_byte;

  len_clie_addr = sizeof(*client);
  dns_header = (t_dns_header *)buffer;
  dns_header->qr = 1;
  dns_header->rcode = 5;
  dns_header->ans_count = 0;

  printf("HELOOLLL all GOOOD\n");
  send_byte = sendto(sockfd, buffer, len_recv, 0, (struct sockaddr *)client, len_clie_addr);
  if (send_byte == -1)
  {
    perror ("sendto errot in send_rec");
  }
}

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*str;
	size_t	i;

	if (!s)
		return (0);
	str = (char*)malloc(sizeof(char) * (len + 1));
	if (str)
	{
		i = 0;
		while (i < len)
			str[i++] = s[start++];
		str[i] = '\0';
		return (str);
	}
	else
		return (NULL);
}

int					check_list(char *str, char **blacklist)
{
	int				i;

	i = 0;
	while (blacklist[i] != NULL)
	{
		if (strstr(blacklist[i], str))
		{
			return (1);
		}
		++i;
	}
	return (0);
}

int					check_blacklist(char *buffer, config_t *data)
{
	char			*str;
	char			*tmp;
	char			*buf;

	buf = &buffer[12]; // skip header datagram;

	str = ft_strsub(buf, 1, strlen(buf));
	tmp = str;

	while (*tmp != '\0')
	{
		if (!isprint(*tmp))
			*tmp = '.';
		tmp++;
	}

	if (check_list(str, data->blacklist))
	{
		// ft_strdel(&str);
    free(str);
    str = NULL;
		return (1); // black_list
	}
	else
	{
    free(str);
    str = NULL;
		// ft_strdel(&str);
		return (0);
	}
}
