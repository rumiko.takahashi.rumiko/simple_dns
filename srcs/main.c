#include "../inc/dns_server.h"

int   count_symbol(const char *str, char symbol)
{
  int i;
  char *tmp;

  i = 0;
  tmp = (char *)str;
  while (*tmp++ != '\0')
    if (*tmp == symbol)
      i++;
  return (i);
}

int read_config(char *name, config_t *config)
{
    char *file;
    int   i;
    int   j;
    // int   flag;
    int   new_line;


    i = 0;
    if ((file = readfile(name)) == NULL)
    {
      printf("Problem with read file\n");
      return (0);
    }
    new_line = count_symbol(file, '\n');
    get_forwarder(config, file);
    if (!(config->blacklist = (char **)malloc(sizeof(char *) * (new_line + 1))))
      return (0);
    config->blacklist[new_line - 1] = NULL;
    while (*file != '\0')
    {
        j = 0;
        if (!(config->blacklist[i] = (char *)malloc(sizeof(char) * MAX_SITE_NAME)))
          return (0);
        memset(config->blacklist[i], 0, MAX_SITE_NAME);
        while (*file != '\n')
          config->blacklist[i][j++] = *file++;
        file++;
        i++;
      }
    return (1);
}

void    get_forwarder(config_t *config, const char *str)
{
  char    *tmp;
  int     i;
  int     j;

  i = 0;
  j = 0;
  tmp = (char *)str;
  memset(config->forwarder, 0, FORWARDE_NAME);
  while (tmp[i] != '\n')
  {
    config->forwarder[j] = tmp[i];
    i++;
    j++;
  }
  config->forwarder[j] = '\0';
}

char  *readfile(char *filename)
{
   char *buffer;
   int string_size;
   int read_size;
   FILE *handler;

   buffer = NULL;
   handler = fopen(filename, "r");
   if (handler)
   {
       fseek(handler, 0, SEEK_END);
       string_size = ftell(handler);
       rewind(handler);
       if (!(buffer = (char *)malloc(sizeof(char) * (string_size + 1))))
         return (NULL);
       read_size = fread(buffer, sizeof(char), string_size, handler);
       buffer[string_size] = '\0';
       if (string_size != read_size)
       {
           free(buffer);
           buffer = NULL;
       }
       fclose(handler);
    }
    return (buffer);
}

int   valid_config(config_t *config)
{
    printf("rep %d\n", valid_forvard(config->forwarder));
    // if (valid_forvard(config->forwarder) == 0)
    //   return (1);
    if (!(valid_blacklist(config->blacklist)))
      return (2);
    return (0);
}

int   valid_blacklist(char **blacklist)
{
  int i = 0;
  blacklist = blacklist + 0;
  return (i++);
}

int valid_forvard(char *forwarder)
{
  int   i;
  int   j;
  int   l;
  char   **check;

  printf("VAlidation forwarder\n");
  if (count_symbol(forwarder, '.') == 3)
  {
    l = 0;
    if (!(check = (char **)malloc(sizeof(char *) * 4)))
      return (0);
    check[4] = NULL;
    i = 0;
    while (forwarder[i] != '\0')
    {
      if (isdigit((int)forwarder[i]))
      {
        j = 0;
        if (!(check[l] = (char *)malloc(sizeof(char) * 4)))
          return (0);
        memset(check[l], 0, 4);
        while(forwarder[i] != '.')
          check[l][j++] = forwarder[i++];
        check[l][j] = '\0';
        printf("check %s\n", check[l]);
        if (j > 3)
          return (0);
      }
      l++;
      i++;
    }
    check[l] = NULL;
    return (1);
  }
  return (1);
}

int main(int ac, char **av)
{
  config_t  config;
  // int   ret;

  if(geteuid() != 0)
  {
    printf("Start with sudo ./demon_server \n");
    return (0);
  }

  /*
  ** fot check config file
  */
  /*
  if (ac == 2)
  {
    printf("ETO 22222\n");
    if (!strcmp(av[1], "config.txt"))
    {
      if (read_config(av[1], &config) == 0)
      {
        printf("Can't read config.txt file\n");
        return (0);
      }
      ret = valid_config(&config);
      if (ret == 1)
      {
        printf("Incorect adrres of forwarder\n");
        return (0);
      }
      if (ret == 2)
      {
        printf("Incoretc blacklist\n");
        return (0);
      }
    }
  }
  else
  {
    printf("Check config.txt file in root off you folder and restart program\n");
    return (0);
  }
  */
  ac = ac +0;
  read_config(av[1], &config);
  start_dns_server(&config);

  return (0);
}
