
#ifndef DNS_SERVER_H
# define DNS_SERVER_H

#include <sys/wait.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<netinet/udp.h>
#include<netinet/ip.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<errno.h>
#include<netdb.h>
#include<sys/types.h>
#include<unistd.h>
#include <fcntl.h>
#include <ctype.h>


#define FORWARDE_NAME 16
#define MAX_SITE_NAME 63
#define PORT     53
#define MAXLINE 512

typedef struct config_s {
  char  forwarder[FORWARDE_NAME];
  char  **blacklist;
}               config_t;

typedef struct			s_dns_header
{
	unsigned short		id;			// identification number			2 byte

	unsigned char		rd :1;		// recursion desired
	unsigned char		tc :1;		// truncated message
	unsigned char		aa :1;		// authoritive answer
	unsigned char		opcode :4;	// purpose of message
	unsigned char		qr :1;		// query/response flag				1 byte

	unsigned char		rcode :4;	// response code
	unsigned char		z :3;		// its z! reserved
	unsigned char		ra :1;		// recursion available				1 byte

	unsigned short		q_count;	// number of question entries		2 byte
	unsigned short		ans_count;	// number of answer entries			2 byte
	unsigned short		auth_count;	// number of authority entries		2 byte
	unsigned short		add_count;	// number of resource entries		2 byte
}						t_dns_header;


/*
** parsing file
*/

int read_config(char *name, config_t *config);
char *readfile(char *filename);
int   count_symbol(const char *str, char symbol);
int   valid_config(config_t *config);
int   valid_blacklist(char **blacklist);
int valid_forvard(char *forwarder);
void    get_forwarder(config_t *config, const char *str);


/*
** function for dns serveer
*/

void  start_dns_server(config_t *config);
// int   check_blacklist(char *buffer, config_t *config);
void    send_rec(int sockfd, char *buffer, int len_recv, struct sockaddr_in *client);
int     query_resend(config_t *data, char *buff, int rec_byte);


int					check_blacklist(char *buffer, config_t *data);
int					check_list(char *str, char **blacklist);
char		*ft_strsub(char const *s, unsigned int start, size_t len);

#endif
